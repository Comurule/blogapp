var express = require('express');
var router  = express.Router();

var {
    article_create_get,
    article_create_post,
    article_delete_get,
    article_delete_post,
    article_update_get,
    article_update_post,
    article_detail,
    article_list
} = require('../controllers/articleController'); 

/// POST ROUTES ///

// GET request for creating a Post. NOTE This must come before routes that display Post (uses id).
router.get('/create', article_create_get);

// POST request for creating Post.
router.post('/create', article_create_post);

// GET request to delete Post.
router.get('/:article_id/delete', article_delete_get);

// POST request to delete Post.
router.post('/:article_id/delete', article_delete_post);

// GET request to update Post.
router.get('/:article_id/update', article_update_get);

// POST request to update Post.
router.post('/:article_id/update', article_update_post);

// GET request for one Post.
router.get('/:article_id', article_detail);

// GET request for list of all Article.
router.get('/', article_list);

module.exports = router;
