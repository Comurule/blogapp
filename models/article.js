'use strict';
module.exports = (sequelize, DataTypes) => {
  var Article = sequelize.define('Article', {
    
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    article_title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        isArray: true,
      }
    },
    article_body: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
      notEmpty: true,
      }
    },
    author_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notEmpty: true,
        isUrl: true,
      }
            
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
    last_modified_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
  });
  // create post association
  // a post will have an author
  // a field called AuthorId will be created in our post table inside the db
  Article.associate = function (models) {
    models.Article.belongsTo(models.Author, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    models.Article.belongsToMany(models.Category,{ 
      as: 'categories', 
      through: 'ArticeCategories',
      foreignKey: 'article_id'
    });
        
    models.Article.hasMany(models.Comment);
  };
  
  
  
  return Article;
};

// Make sure you complete other models fields