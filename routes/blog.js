var express = require('express');
var router = express.Router();

var article = require('./article');
var author = require('./author');
var category = require('./category');
var comment = require('./comment');

router.use('/article', article);
router.use('/author', author);
router.use('/category', category);
router.use('/comment', comment);

// Require our controllers.
var { index } = require('../controllers/articleController'); 

// GET blog home page.
router.get('/', index); 

// export all the router created
module.exports = router;
