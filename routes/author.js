var express = require('express');
var router  = express.Router();

var {
    author_create_get,
    author_create_post,
    author_delete_get,
    author_delete_post,
    author_update_get,
    author_update_post,
    author_detail,
    author_list
} = require('../controllers/authorController');

/// AUTHOR ROUTES ///

// GET request for creating Author. NOTE This must come before route for id (i.e. display author).
router.get('/create', author_create_get);

// POST request for creating Author.
router.post('/create', author_create_post);

// GET request to delete Author.
router.get('/:author_id/delete', author_delete_get);

// POST request to delete Author
router.post('/:author_id/delete', author_delete_post);

// GET request to update Author.
router.get('/:author_id/update', author_update_get);

// POST request to update Author.
router.post('/:author_id/update', author_update_post);

// GET request for one Author.
router.get('/:author_id', author_detail);

// GET request for list of all Authors.
router.get('/', author_list);


module.exports = router;