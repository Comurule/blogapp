var express = require('express');
var router = express.Router();

var {
    category_create_get,
    category_create_post,
    category_delete_get,
    category_delete_post,
    category_update_get,
    category_update_post,
    category_detail,
    category_list
    
} = require('../controllers/categoryController');

/// Category ROUTES ///

// GET request for creating a Category. NOTE This must come before route that displays Category (uses id).
router.get('/create', category_create_get);

// POST request for creating Category.
router.post('/create', category_create_post);

// GET request to delete Category.
router.get('/:category_id/delete', category_delete_get);

// POST request to delete Category.
router.post('/:category_id/delete', category_delete_post);

// GET request to update Category.
router.get('/:category_id/update', category_update_get);

// POST request to update Category.
router.post('/:category_id/update', category_update_post);

// GET request for one Category.
router.get('/:category_id', category_detail);

// GET request for list of all Categories.
router.get('/', category_list);


module.exports = router;