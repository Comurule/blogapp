var express = require('express');
var router = express.Router();

var {
    comment_create_get,
    comment_create_post,
    comment_delete_get,
    comment_delete_post,
    comment_update_get,
    comment_update_post,
    comment_detail,
    comment_list
} = require('../controllers/commentController');

/// COMMENT ROUTES ///

// GET request for creating Comment. NOTE This must come before route for id (i.e. display comment).
router.get('/create', comment_create_get);

// POST request for creating Comment.
router.post('/create', comment_create_post);

// GET request to delete Comment.
router.get('/:comment_id/delete', comment_delete_get);

// POST request to delete Comment
router.post('/:comment_id/delete', comment_delete_post);

// GET request to update Comment.
router.get('/:comment_id/update', comment_update_get);

// POST request to update Comment.
router.post('/:comment_id/update', comment_update_post);

// GET request for one Comment.
router.get('/:comment_id', comment_detail);

// GET request for list of all Comments.
router.get('', comment_list);

module.exports = router;