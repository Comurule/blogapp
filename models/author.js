'use strict';
module.exports = (sequelize, DataTypes) => {
  var Author = sequelize.define('Author', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate:{
        isAlpha: true,
        notEmpty: true,
      }
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate:{
        isAlpha: true,
        notEmpty: true,
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true,
        isEmail: true,
        isLowercase: true,
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
        validate: {
          notEmpty: true,
          isAlphanumeric: true,
          len: [8,20], //password cant be less than 8 characters                    
        }
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'editor'//everyone is a reader, so there wont be such a role (only admin and editor)
      },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
    last_modified_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
    
  });

  // create association between author and post
  // an author can have many posts
  Author.associate = function(models) {
    models.Author.hasMany(models.Article);
  };
  
  return Author;
};
 