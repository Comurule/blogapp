'use strict';
module.exports = (sequelize, DataTypes) => {
  var Category = sequelize.define('Category', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
    last_modified_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
  });
 
  // create association between category and post
  // an category can have many posts
  Category.associate = function(models) {
    models.Category.belongsToMany(models.Article,{ 
      as: 'articles', 
      through: 'ArticeCategories',
      foreignKey: 'category_id'
      
    });
    
    models.Category.belongsToMany(models.Author,{ 
      as: 'authors', 
      through: 'AuthorCategories',
      foreignKey: 'category_id'
      
    });
  };
  
  return Category;
};

 


// Make sure you complete other models fields