'use strict';
module.exports = (sequelize, DataTypes) => {
  var Comment = sequelize.define('Comment', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    comment_body: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
      notEmpty: true,
      }
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
    last_modified_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('NOW()'),
    },
  });
  
  Comment.associate = function (models) {
  models.Comment.belongsTo(models.Article, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
  };
  
  return Comment;
};

// Make sure you complete other models fields