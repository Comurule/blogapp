var models = require('../models');

var async = require('async');

// Display post create form on GET.
exports.article_create_get = function(req, res, next) {
        // renders a post form
        res.render('forms/article_form', { title: 'Create Article', layout: 'layouts/detail'});
        console.log("Article form renders successfully");
};

// Handle post create on POST.
exports.article_create_post = async function( req, res, next) {

     // create a new post based on the fields in our post model
     // I have create two fields, but it can be more for your model
     
     const article = await models.Article.create({
            article_title: req.body.article_title,
            article_body: req.body.article_body,
            AuthorId: req.body.author_id} 
     );
        
     console.log("The saved article " + article.id);
     
     const category = await models.Category.findById(req.body.category_id);
     
     if (!category) {
          return res.status(400);
     }

     // ok category exist
     console.log("This is the category name we entered in front end " + category.name)
    
    const article_category = {
      artice_id: article.id,
      category_id: category.id
    };
    
     // Create and save a productOrder
    //  const ProductCategorySaved = await models.PostCategory.create(post_category);
     await article.addCategory(category);

     console.log("Article created successfully");
     // check if there was an error during post creation
     res.redirect('/blog/article');
         
};

//  Promise.all([User.create(), City.create()])
//     .then(([user, city]) => UserCity.create({userId: user.id, cityId: city.id}))


// Display post delete form on GET.
exports.article_delete_get = function(req, res, next) {
       models.Article.destroy({
            // find the post_id to delete from database
            where: {
              id: req.params.article_id
            }
          }).then(function() {
           // If an post gets deleted successfully, we just redirect to posts list
           // no need to render a page
            res.redirect('/blog/article');
            console.log("Article deleted successfully");
          });
};

// Handle post delete on POST.
exports.article_delete_post = function(req, res, next) {
          models.Article.destroy({
            // find the post_id to delete from database
            where: {
              id: req.params.article_id
            }
          }).then(function() {
           // If an post gets deleted successfully, we just redirect to posts list
           // no need to render a page
            res.redirect('/blog/article');
            console.log("Article deleted successfully");
          });

 };

// Display post update form on GET.
exports.article_update_get = function(req, res, next) {
        // Find the post you want to update
        console.log("ID is " + req.params.article_id);
        models.Article.findById(
                req.params.article_id
        ).then(function(article) {
               // renders a post form
               res.render('forms/article_form', { title: 'Update Article', article: article, layout: 'layouts/detail'});
               console.log("Article update get successful");
          });
        
};

// Handle post update on POST.
exports.article_update_post = function(req, res, next) {
        console.log("ID is " + req.params.article_id);
        models.Article.update(
        // Values to update
            {
                article_title: req.body.article_title,
                article_body: req.body.article_body
            },
          { // Clause
                where: 
                {
                    id: req.params.article_id
                }
            }
        //   returning: true, where: {id: req.params.post_id} 
         ).then(function() { 
                // If an post gets updated successfully, we just redirect to posts list
                // no need to render a page
                res.redirect("/blog/article");  
                console.log("Article updated successfully");
          });
};

// Display detail page for a specific post.
exports.article_detail = function(req, res, next) {
        // find a post by the primary key Pk
        models.Article.findById(
                req.params.article_id,
                {
                    include: models.Comment,
                    
                }
                
        ).then(function(article) {
        // renders an inividual post details page
        res.render('pages/article_detail', { title: 'Article Details', article: article, layout: 'layouts/detail'} );
        console.log("Article details renders successfully");
        });
};
 
// Display list of all posts.
exports.article_list = function(req, res, next) {
        // controller logic to display all posts
        models.Article.findAll({
              // Make sure to include the products
                include: [{
                  model: models.Category,
                  as: 'categories',
                  required: false,
                  // Pass in the Product attributes that you want to retrieve
                  attributes: ['id', 'name'],
                  through: {
                    // This block of code allows you to retrieve the properties of the join table
                    model: models.ArticleCategory 
                  }
                }]
        }).then(function(articles) {
        // renders a post list page
        console.log("rendering post list");
        res.render('pages/article_list', { title: 'Post List', articles: articles, layout: 'layouts/list'} );
        console.log("Articles list renders successfully");
        });
        
};

// This is the blog homepage.
exports.index = function(req, res) {

      
   // find the count of posts in database
      models.Article.findAndCountAll(
      ).then(function(articleCount)
      {
        models.Author.findAndCountAll(
      ).then(function(authorCount)
      {
        models.Comment.findAndCountAll(
      ).then(function(commentCount)
      {
        models.Category.findAndCountAll(
      ).then(function(categoryCount)
      {
       
        
 
        res.render('pages/index', {title: 'Homepage', articleCount: articleCount, authorCount: authorCount, commentCount: commentCount, categoryCount: categoryCount, layout: 'layouts/main'});
        
        // res.render('pages/index_list_sample', { title: 'Post Details', layout: 'layouts/list'});
        // res.render('pages/index_detail_sample', { page: 'Home' , title: 'Post Details', layout: 'layouts/detail'});

      });
      });
      });
      });
       
      
    
    
    };


 